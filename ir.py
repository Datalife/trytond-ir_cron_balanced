# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import logging
import time
from ast import literal_eval
from datetime import datetime

from trytond import backend
from trytond.config import config
from trytond.pool import PoolMeta
from trytond.pyson import And, Eval, If
from trytond.transaction import Transaction
from trytond.worker import run_task

logger = logging.getLogger(__name__)


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def run(cls, db_name):
        """ Override completely the core method """
        """ TODO: Check after each migration"""

        logger.info('cron started for "%s"', db_name)
        now = datetime.now()
        retry = config.getint('database', 'retry')

        def convert_ids(value):
            new_value = value or []
            if new_value:
                new_value = literal_eval(new_value)
            if isinstance(new_value, int):
                new_value = [new_value]
            return new_value

        skip_ids = convert_ids(config.get('cron', 'exclude', default=[]))
        valid_ids = convert_ids(config.get('cron', 'include', default=[]))
        # do not allow to define include and exclude params
        # if define include, the other crons are implicitly excluded
        # (and viceversa)
        assert not bool(skip_ids and valid_ids)

        with Transaction().start(db_name, 0) as transaction:
            # do not lock table
            domain = [['OR',
                ('next_call', '<=', now),
                ('next_call', '=', None),
            ]]
            if skip_ids:
                domain.append(('id', 'not in', skip_ids))
            if valid_ids:
                domain.append(('id', 'in', valid_ids))
            crons = cls.search(domain)

            for cron in crons:
                logger.info("Run cron %s", cron.id)
                for count in range(retry, -1, -1):
                    if count != retry:
                        time.sleep(0.02 * (retry - count))
                    try:
                        cron.run_once()
                        cron.next_call = cron.compute_next_call(now)
                        cron.save()
                        transaction.commit()
                        logger.info("Finished cron %s", cron.id)
                    except Exception as e:
                        transaction.rollback()
                        if (isinstance(e, backend.DatabaseOperationalError)
                                and count):
                            continue
                        logger.error('Running cron %s', cron.id, exc_info=True)
                    break
        while transaction.tasks:
            task_id = transaction.tasks.pop()
            run_task(db_name, task_id)
        logger.info('Finished running cron tasks for "%s"', db_name)

    @classmethod
    def view_attributes(cls):
        visual_cond = If(
            And(
                Eval('active', False),
                Eval('next_call', datetime.min) < datetime.now()),
            'danger',
            '')
        return super().view_attributes() + [
            ('//field[@name="next_call"]', 'visual', visual_cond)]
